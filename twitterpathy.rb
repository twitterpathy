#!/usr/bin/env ruby

require 'rubygems'
gem('twitter4r', '>=0.2.1')
require 'twitter'
require 'dbus'

CONN_MGR_PARAM_FLAG_REQUIRED = 1
CONN_MGR_PARAM_FLAG_REGISTER = 2
CONN_MGR_PARAM_FLAG_HAS_DEFAULT = 4
CONN_MGR_PARAM_FLAG_SECRET = 8

HANDLE_TYPE_NONE = 0
HANDLE_TYPE_CONTACT = 1
HANDLE_TYPE_ROOM = 2
HANDLE_TYPE_LIST = 3
HANDLE_TYPE_GROUP = 4

CONNECTION_STATUS_CONNECTED = 0
CONNECTION_STATUS_CONNECTING = 1
CONNECTION_STATUS_DISCONNECTED = 2

CONNECTION_STATUS_REASON_NONE_SPECIFIED = 0
CONNECTION_STATUS_REASON_REQUESTED = 1
CONNECTION_STATUS_REASON_NETWORK_ERROR = 2
CONNECTION_STATUS_REASON_AUTHENTICATION_FAILED = 3
CONNECTION_STATUS_REASON_ENCRYPTION_ERROR = 4
CONNECTION_STATUS_REASON_NAME_IN_USE = 5
CONNECTION_STATUS_REASON_CERT_NOT_PROVIDED = 6
CONNECTION_STATUS_REASON_CERT_UNTRUSTED = 7
CONNECTION_STATUS_REASON_CERT_EXPIRED = 8
CONNECTION_STATUS_REASON_CERT_NOT_ACTIVATED = 9
CONNECTION_STATUS_REASON_CERT_HOSTNAME_MISMATCH = 10
CONNECTION_STATUS_REASON_CERT_FINGERPRINT_MISMATCH = 11
CONNECTION_STATUS_REASON_CERT_SELF_SIGNED = 12
CONNECTION_STATUS_REASON_CERT_OTHER_ERROR = 13

# TODO: other flags in this set
CHANNEL_GROUP_FLAG_PROPERTIES = 2048

#Handles used for contact lists, of type HANDLE_TYPE_LIST
LIST_HANDLE_FOLLOWERS = 1
LIST_HANDLE_FRIENDS = 2

class Twitterpathy < DBus::Object
	dbus_interface "org.freedesktop.Telepathy.ConnectionManager" do
		dbus_method :GetParameters, "in proto:s, out parameters:a(susv)" do |proto|
			puts "GetParameters(#{proto.inspect})"
			$stderr.puts "Attempt to use unsupported protocol #{proto}" if proto != 'twitter'
			
			parameters = [['account', CONN_MGR_PARAM_FLAG_REQUIRED | CONN_MGR_PARAM_FLAG_REGISTER, 's', ['s', '']], ['password', CONN_MGR_PARAM_FLAG_REQUIRED | CONN_MGR_PARAM_FLAG_REGISTER | CONN_MGR_PARAM_FLAG_SECRET, 's', ['s', '']]]
			[parameters]
		end
		
		dbus_method :ListProtocols, "out protocols:as" do ||
			puts "ListProtocols"
			protocols = ['twitter']
			[protocols]
		end
		
		dbus_method :RequestConnection, "in proto:s, in parameters:a{sv}, out service:s, out object:o" do |proto, parameters|
			puts "RequestConnection(#{proto.inspect}, #{parameters.inspect})"
			$stderr.puts "Attempt to use unsupported protocol #{proto}" if proto != 'twitter'
			
			object = "/org/freedesktop/Telepathy/Connection/Twitterpathy/twitter/#{parameters['account']}"
			service = "org.freedesktop.Telepathy.Connection.Twitterpathy.twitter.#{parameters['account']}"
			
			#Register a new D-BUS service for this connection
			connection_service = $bus.request_service(service)
			
			#Create the connection object, and export it on the new service
			connection = TwitterConnection.new(object, parameters['account'], parameters['password'], connection_service)
			connection_service.export(connection)
			
			#Send signal
			NewConnection(service, object, 'twitter')
			
			[service, object]
		end
		
		dbus_signal :NewConnection, 'bus_name:s, object_path:o, proto:s'
	end
end

class TwitterConnection < DBus::Object
	attr_reader :me, :myfollowerhandles
	
	def initialize(path, account, password, connection_service)
		puts "initialize TwitterConnection(#{path}, #{account}, ***, #{connection_service})"
		@account = account
		@password = password
		@connection_service = connection_service
		@status = CONNECTION_STATUS_CONNECTING
		@handles = {HANDLE_TYPE_NONE => {}, HANDLE_TYPE_CONTACT => {}, HANDLE_TYPE_ROOM => {}, HANDLE_TYPE_LIST => {}, HANDLE_TYPE_GROUP => {}}
		super(path)
	end
	
	dbus_interface 'org.freedesktop.Telepathy.Connection' do
		dbus_method :Connect, '' do ||
			puts "Connect, account=#{@account}"
			
			@twitter_client = Twitter::Client.new(:login => @account, :password => @password)
			
			@me = @twitter_client.my(:info)
			@handles[HANDLE_TYPE_CONTACT][@me.id] = @me
			
			puts "Connected as #{@me.inspect}"
			
			#Load followers
			myfollowers = @twitter_client.my(:followers)
			@myfollowerhandles = myfollowers.map{|follower| follower.id }
			myfollowers.each do |follower|
				@handles[HANDLE_TYPE_CONTACT][follower.id] = follower
			end
			
			followers_channel_object = "/org/freedesktop/Telepathy/Connection/Twitterpathy/twitter/#{@account}/contactlist/followers"
			followers_channel = FollowersChannel.new(followers_channel_object, self)
			@connection_service.export(followers_channel)
			
			@channels = [[followers_channel_object, 'org.freedesktop.Telepathy.Channel.Type.ContactList', HANDLE_TYPE_LIST, LIST_HANDLE_FOLLOWERS]]
			@handles[HANDLE_TYPE_LIST][LIST_HANDLE_FOLLOWERS] = followers_channel
			
			@status = CONNECTION_STATUS_CONNECTED
			
			StatusChanged(@status, CONNECTION_STATUS_REASON_REQUESTED)
			NewChannel(followers_channel_object, 'org.freedesktop.Telepathy.Channel.Type.ContactList', HANDLE_TYPE_LIST, LIST_HANDLE_FOLLOWERS, false)
		end
		
		dbus_method :Disconnect, '' do ||
			puts 'Disconnect'
			
			@status = CONNECTION_STATUS_DISCONNECTED
			
			StatusChanged(@status, CONNECTION_STATUS_REASON_REQUESTED)
		end
		
		dbus_method :GetInterfaces, 'out interfaces:as' do ||
			interfaces = []
			[interfaces]
		end
		
		dbus_method :GetProtocol, 'out protocol:s' do ||
			protocol = 'twitter'
			[protocol]
		end
		
		dbus_method :GetSelfHandle, 'out self_handle:u' do ||
			self_handle = @me.id
			[self_handle]
		end
		
		dbus_method :GetStatus, 'out status:u' do ||
			[@status]
		end
		
		dbus_method :HoldHandles, 'in handle_type:u, in handles:au' do |handle_type, handles|
			# TODO
		end
		
		dbus_method :InspectHandles, 'in handle_type:u, in handles:au, out strings:as' do |handle_type, handles|
			strings = handles.map{|handle| @handles[handle_type][handle].inspect }
			[strings]
		end
		
		dbus_method :ListChannels, 'out channels:a(osuu)' do ||
			[@channels]
		end
		
		dbus_method :ReleaseHandles, 'in handle_type:u, in handles:au' do |handle_type, handles|
			# TODO
		end
		
		dbus_method :RequestChannel, 'in type:s, in handle_type:u, in handle:u, in suppress_handler:b, out channel:o' do |type, handle_type, handle, suppress_handler|
			if type == 'org.freedesktop.Telepathy.Channel.Type.ContactList' && handle_type == HANDLE_TYPE_LIST && handle == LIST_HANDLE_FOLLOWERS
				channel = "/org/freedesktop/Telepathy/Connection/Twitterpathy/twitter/#{@account}/contactlist/followers"
			else
				# TODO: throw org.freedesktop.Telepathy.Error.NotImplemented or org.freedesktop.Telepathy.Error.InvalidHandle
				channel = ''
			end
			[channel]
		end
		
		dbus_method :RequestHandles, 'in handle_type:u, in names:as, out handles:au' do |handle_type, names|
			puts "RequestHandles(#{handle_type.inspect}, #{names.inspect})"
			
			if handle_type == HANDLE_TYPE_CONTACT
				handles = names.map {|name|
					# TODO: make this more efficient?
					contact_handle = nil
					@handles[HANDLE_TYPE_CONTACT].each do |handle, contact|
						if contact.name == name
							contact_handle = handle
							break
						end
					end
					
					if contact_handle.nil?
						contact = @twitter_client.user(name) # TODO: handle errors
						@handles[HANDLE_TYPE_CONTACT][contact.id] = contact
						contact_handle = contact.id
					end
					
					contact_handle
				}
			elsif handle_type = HANDLE_TYPE_LIST
				handles = names.map {|name|
					if name == 'publish'
						LIST_HANDLE_FOLLOWERS
					else
						# TODO
						0
					end
				}
			else
				# TODO: other types
				handles = []
			end
			
			[handles]
		end
		
		dbus_signal :NewChannel, 'object_path:o, channel_type:s, handle_type:u, handle:u, suppress_handler:b'
		dbus_signal :StatusChanged, 'status:u, reason:u'
	end
end

class FollowersChannel < DBus::Object
	def initialize(path, connection)
		@connection = connection
		@properties = {
			'org.freedesktop.Telepathy.Channel' => {
				'ChannelType' => ['s', 'org.freedesktop.Telepathy.Channel.Type.ContactList'],
				'Interfaces' => ['as', ['org.freedesktop.Telepathy.Channel.Interface.Group']],
				'TargetHandle' => ['u', LIST_HANDLE_FOLLOWERS],
				'TargetID' => ['s', 'Followers'],
				'TargetHandleType' => ['u', HANDLE_TYPE_LIST]
			},
			'org.freedesktop.Telepathy.Channel.Interface.Group' => {
				'GroupFlags' => ['u', CHANNEL_GROUP_FLAG_PROPERTIES],
				'HandleOwners' => ['a{uu}', {}],
				'LocalPendingMembers' => ['a(uuus)', []],
				'Members' => ['au', connection.myfollowerhandles],
				'RemotePendingMembers' => ['au', []],
				'SelfHandle' => ['u', connection.me.id]
			}
		}
		super(path)
	end
	
	dbus_interface 'org.freedesktop.Telepathy.Channel' do
		dbus_method :Close, '' do ||
			# TODO: throw org.freedesktop.Telepathy.Error.NotImplemented
		end
		
		#Deprecated methods, just return appropriate properties
		dbus_method :GetChannelType, 'out ChannelType:s' do ||
			[@properties['org.freedesktop.Telepathy.Channel']['ChannelType'][1]]
		end
		
		dbus_method :GetHandle, 'out TargetHandleType:u, out TargetHandle:u' do ||
			[@properties['org.freedesktop.Telepathy.Channel']['TargetHandleType'][1], @properties['org.freedesktop.Telepathy.Channel']['TargetHandle'][1]]
		end
		
		dbus_method :GetInterfaces, 'out Interfaces:as' do ||
			[@properties['org.freedesktop.Telepathy.Channel']['Interfaces'][1]]
		end
		
		dbus_signal :Closed, ''
	end
	
	# TODO: make a nice general way to add properties to classes
	dbus_interface 'org.freedesktop.DBus.Properties' do
		dbus_method :Get, 'in interface_name:s, in property_name:s, out value:v' do |interface_name, property_name|
			value = @properties[interface_name][property_name]
			[value]
		end
		
		dbus_method :Set, 'in interface_name:s, in property_name:s, in value:v' do |interface_name, property_name, value|
		end
		
		dbus_method :GetAll, 'in interface_name:s, out props:a{sv}' do |interface_name|
			props = @properties[interface_name]
			[props]
		end
	end
	
	dbus_interface 'org.freedesktop.Telepathy.Channel.Type.ContactList' do
	end
	
	dbus_interface 'org.freedesktop.Telepathy.Channel.Interface.Group' do
		dbus_method :AddMembers, 'in contacts:au, in message:s' do |contacts, message|
			# TODO
		end
		
		dbus_method :RemoveMembers, 'in contacts:au, in message:s' do |contacts, message|
			# TODO
		end
		
		dbus_method :RemoveMembersWithReason, 'in contacts:au, in message:s, in reason:u' do |contacts, message, reason|
			# TODO
		end
		
		#Deprecated methods
		dbus_method :GetGroupFlags, 'out GroupFlags:u' do ||
			[@properties['org.freedesktop.Telepathy.Channel.Interface.Group']['GroupFlags'][1]]
		end
		
		dbus_method :GetLocalPendingMembers, 'out LocalPendingMembers:au' do ||
			[@properties['org.freedesktop.Telepathy.Channel.Interface.Group']['LocalPendingMembers'][1].map{|member| member[0]}]
		end
		
		dbus_method :GetLocalPendingMembersWithInfo, 'out LocalPendingMembers:a(uuus)' do ||
			[@properties['org.freedesktop.Telepathy.Channel.Interface.Group']['LocalPendingMembers'][1]]
		end
		
		dbus_method :GetMembers, 'out Members:au' do ||
			puts "GetMembers, returning #{@properties['org.freedesktop.Telepathy.Channel.Interface.Group']['Members'][1].inspect}"
			[@properties['org.freedesktop.Telepathy.Channel.Interface.Group']['Members'][1]]
		end
		
		dbus_method :GetRemotePendingMembers, 'out RemotePendingMembers:au' do ||
			[@properties['org.freedesktop.Telepathy.Channel.Interface.Group']['RemotePendingMembers'][1]]
		end
		
		dbus_method :GetSelfHandle, 'out SelfHandle:u' do ||
			[@properties['org.freedesktop.Telepathy.Channel.Interface.Group']['SelfHandle'][1]]
		end
		
		dbus_signal :HandleOwnersChanged, 'added:a{uu}, removed:au'
		dbus_signal :SelfHandleChanged, 'self_handle:u'
		dbus_signal :GroupFlagsChanged, 'added:u, removed:u'
		dbus_signal :MembersChanged, 'message:s, added:au, removed:au, local_pending:au, remote_pending:au, actor:u, reason:u'
	end
end

$bus = DBus::SessionBus.instance

service = $bus.request_service("org.freedesktop.Telepathy.ConnectionManager.Twitterpathy")

exported_obj = Twitterpathy.new("/org/freedesktop/Telepathy/ConnectionManager/Twitterpathy")
service.export(exported_obj)

puts "listening"
main = DBus::Main.new
main << $bus
main.run
