#!/usr/bin/env ruby

require 'rubygems'
gem('twitter4r', '>=0.2.1')
require 'twitter'

client = Twitter::Client.new(:login => 'pathy_test', :password => 'pathpass')

# I want to post a new status to my timeline, which can also be do by:
#  client.status(:post, 'My new status message')
#status = Twitter::Status.create(:client => client, :text => 'My new status message')

#user = Twitter::User.find('dictionary', client)
#message = Twitter::Message.create(:client => client, :recipient => user, :text => 'canadaphile')

followers = client.my(:followers)
puts followers.inspect
friends = client.my(:friends)
puts friends.inspect
